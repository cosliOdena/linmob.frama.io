+++
title = "Weekly Update (13/2022): SailfishOS 4.4, phoc 0.13.1, JumpDrive but implemented in Go and lots of PinePhone Pro videos!"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro", "LinuxPhoneApps","postmarketOS podcast"]
categories = ["weekly update"]
authors = ["peter"]
+++


Also, LinuxPhoneApps.org is finally live and lists over 360 apps!
<!-- more -->

_Commentary in italics._

### DIY PinePhone Hardware enhancements
* xnux.eu log (megi): [Keyboard light](https://xnux.eu/log/#064). _Reminds me of IBM's ThinkLight, great idea!_

### Software progress

#### GNOME ecosystem
* phoc 0.13.1 is a minor release, read the [full release notes for details](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.13.1).
* This Week in GNOME: [#37 Absolutely Serious!](https://thisweek.gnome.org/posts/2022/04/twig-37/). _Nice progress!_

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Progress on gestures and 15-minute bugs!](https://pointieststick.com/2022/03/26/this-week-in-kde-progress-on-gestures-and-15-minute-bugs/). _Gestures are sure welcome for everyone who tries to use Plasma Desktop on a tablet!_
* Qt blog: [Shell Extensions in Qt Wayland 6.3](https://www.qt.io/blog/shell-extensions-in-qt-wayland-6.3).

#### Software projects
* [u-pine](https://github.com/yurinnick/u-pine), a go based JumpDrive alternative.

#### Sailfish OS
+ Jolla blog: [Sailfish OS Vanha Rauma brings in several new features and improvements](https://blog.jolla.com/vanharauma/). _It's out of Early Access!_

#### UBports
* UBports blog: [TET-Update 2 aka 0010: Learn to set up your develop environment for Ubuntu Touch apps](https://ubports.com/blog/ubports-blogs-nachrichten-1/post/tet-update-2-aka-0010-learn-to-set-up-your-develop-environment-for-ubuntu-touch-apps-3843).


### Worth reading

#### Retro handhelds
* TuxPhones: [Two new Linux handhelds with nostalgic vibes](https://tuxphones.com/risc-v-clockworkpi-devterm-penkesu-raspberry-pi-zero-2w-pda-linux-nostalgia/).

#### Check the date
* PINE64: [Introducing the PineBuds and PinePod. Seriously.](https://www.pine64.org/2022/04/01/introducing-the-pinebuds-and-pinepod-seriously/).

#### Infrastructure
* Drew DeVault: [It is important for free software to use free software infrastructure](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html).

#### App lists and such
* LinuxPhoneApps: [Hello World 👋](https://linuxphoneapps.org/blog/its-better-to-launch/). _There's still a lot to do, with to do items well beyond what's in the issue tracker. Let me know what you think!_

#### Competition :)
* FossPhones: [Linux Phone News - March 29, 2022](https://fossphones.com/03-29-22.html)

### Worth listening
* postmarketOS podcast: [#16 SDM845, mrtest, PinePhone keyboard, Chromebooks, PowerVR](https://cast.postmarketos.org/episode/16-SDM845-mrtest-PinePhone-keyboard-Chromebooks-PowerVR/). _Another great episode of my favorite Mobile Linux podcast!_

### Worth watching

#### Libre Planet Sxmo
* Anjandev Momi: [Sxmo: Freedom on mobile devices through simplicity and hackability](https://media.libreplanet.org/u/libreplanet/m/sxmo-freedom-on-mobile-devices-through-simplicity-and-hackability/). _Great Talk!_

#### PinePhone Pro with Phosh
* Wolf Fur Programming: [Phosh Pine phone Pro](https://www.youtube.com/watch?v=pVoTF672IS0). _Nice video of Manjaro Phosh!_
* socketwench: [PinePhone Pro and Pine Keyboard Week 1 Review](https://www.youtube.com/watch?v=_A66uTKt_so). _
* Bitter Epic Productions: [Installing Phosh on the PinePhone Pro: WHAT YOU SHOULD KNOW](https://www.youtube.com/watch?v=pLUviM3ARSs). _So funny!_

#### Mobian vs Droidian
* Linux Stuff: [PinePhone Pro (Mobian) vs Pixel 3a (Droidian)](https://www.youtube.com/watch?v=gYeihbe4Zhk). _Interesting comparison!_

#### Adjusting Phosh for the Keyboard
* (RTP) Privacy Tech Tips: [Phosh Changing Scale For More Desktop Look](https://www.youtube.com/watch?v=IYzubpPn57g). _Nice!_

#### Mobian on Pocophone F1
* Ivon Huang: [手機Linux系統: Mobian (Debian) on Pocophone F1](https://www.youtube.com/watch?v=sDW3bcCshlw).

#### PinePhone Pro Dev Stream
* caleb: [PinePhone Pro kernel dev / bits and bobs / Q&A](https://www.youtube.com/watch?v=7wwmUvCvKQY)

#### PinePhone gaming
* Supositware: [Doom 3 (dhewm3) on the PinePhone](https://www.youtube.com/watch?v=hAmLCxF-Xxg).

#### Something fun
* baby WOGUE: [GNOME 4 will come with improved consistency (beyond software)](https://www.youtube.com/watch?v=zoYUMaK4k9A). _Sounds sane!_

#### Shorts
* Drew Naylor 2: [RetiledActionCenter demo #1: Flashlight Toggle Showcase](https://www.youtube.com/shorts/_xcpuhhv5TE).


### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
