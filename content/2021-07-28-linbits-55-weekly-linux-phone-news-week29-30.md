+++
title = "LinBits 55: Weekly Linux Phone news / media roundup (week 29/30)"
date = "2021-07-28T21:49:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "Sailfish OS", "Ubuntu Touch", "LINMOBapps",]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's Wednesday. Now what happened since last Wednesday?_

Nemo Mobile and WayDroid progress further, UBports plans for VoLTE, LINMOBapps hits 300 apps and more!<!-- more --> _Commentary in italics._ 

### Software releases
* Jozef Mlich set up a place to download current [Nemo Mobile images for PinePhone (based on Manjaro)](https://github.com/jmlich/nemo-images).

### Worth noting
* If you are having trouble with choosing the correct icon set on a system with a mixture of GTK and Qt/Kirigami apps, and would just love to set them to a native one each, [here's something quite helpful](https://twitter.com/nicofeee/status/1420047088767877123).
* [Maemo Leste has been ported to the 2011 Motorola Droid 3](https://twitter.com/maemoleste/status/1418617227209752578).

### Worth reading

#### Nemo Mobile awesomeness

* Jozef Mlich: [Nemomobile in July/2021 part two](https://blog.mlich.cz/2021/07/nemomobile-in-july-2021-part-two/). _Great to see NemoMobile progress continue!_

#### Video recording
* Linux Smartphones: [A clunky (but useable) method for recording video on a PinePhone](https://linuxsmartphones.com/a-clunky-but-useable-method-for-recording-video-on-a-pinephone/).

#### Ubuntu Touch
* UBports News: [First steps in VoLTE support for Ubuntu Touch](https://ubports.com/blog/ubports-news-1/post/first-steps-in-volte-support-for-ubuntu-touch-3768). _This is very important, not just for North America!_
  * Linux Smartphones: [UBPorts plans to bring VoLTE and Voice over WiFi support to Ubuntu Touch](https://linuxsmartphones.com/ubports-plans-to-bring-volte-and-voice-over-wifi-support-to-ubuntu-touch/).

#### Kalender and Tok progress
* Claudio Cambra: [Ticking this off the todo list — Kalendar week 7 (GSoC 2021)](https://claudiocambra.com/2021/07/25/ticking-this-off-the-todo-list-kalendar-week-7-gsoc-2021/). _Nice progess!_
* Janet Blackquill: [This Week In Tok: Days Of Work, Seconds Of Experience](https://blog.blackquill.cc/this-week-in-tok-days-of-work-seconds-of-experience). _Same here!_

#### Purism
* Purism: [Librem 5 File Transfer](https://puri.sm/posts/librem-5-file-transfer/). _I need to try that app!_

#### PinePhone keyboard
* xnux.eu: [The latest Pinephone keyboard prototype](https://xnux.eu/log/#043). _Another great, in-depth post by Megi on the upcoming PinePhone keyboard accessory._

#### WayDroid (formerly known as "new Anbox" or "anbox-halium") 
* TuxPhones: [OnePlus 6/6T Linux port now supports lag-free Android app integration thanks to WayDroid](https://tuxphones.com/waydroid-anbox-linux-mainline-oneplus-6-6t-postmarketos/). _This is really great, but keep in mind: It's not quite ready for end-users yet._
* Linux Smartphones: [WayDroid lets you run Android apps on Linux phones (with smoother performance than Anbox)](https://linuxsmartphones.com/waydroid-lets-you-run-android-apps-on-linux-phones-with-smoother-performance-than-anbox/).

#### Competition ;-)
* Linux Smartphones: [News roundup: JingPad A1 Linux tablet, PinePhone keyboard, and more](https://linuxsmartphones.com/news-roundup-jingpad-a1-linux-tablet-pinephone-keyboard-and-more/). _Nice roundup!_

### Worth watching

#### Introductions
* HomebrewXS: [SupernovaOS introduction](https://www.youtube.com/watch?v=VpadcM-_deA). 

#### Tutorials
* kdlk.de: tech: [PinePhone App Development - Successful Cross Compilation x86_64 to ARM #01 Rust + GTK](https://www.youtube.com/watch?v=D8ddxyS6Fqk). _Nice video! The complications of cross compiling have me compiling apps just on my ARM devices ;-) - and if that takes too long, [distcc] is your friend._
* SSTec Tutorials: [How To Install Ubuntu Touch OS on Your Android Phone Or Tablet!](https://www.youtube.com/watch?v=5nA4ZJR9jdo). _Simple, right? The most annoying part will be unlocking those Xiaomi Phones :)_

#### WayDroid
* Caleb Connolly: [#shorts | crossy road on my Linux phone (OnePlus 6 with mainline and #postmarketOS](https://www.youtube.com/watch?v=I9qaD5YIPkc). _The colors are off, but it's still amazing!_

#### Librem 5 corner
* Purism: [Librem 5 File Transfer](https://www.youtube.com/watch?v=OaX9L85jhuM).

#### JingOS
* TechHut: [JingPad A1 - Hands On Review - ARM Linux Tablet with JingOS (Alpha/DVT)](https://www.youtube.com/watch?v=Rq6TE2nZz3M). _Great unboxing and first impression video! Sadly, as neofetch reveals, this tablet runs Linux 4.14, which makes me quite sure that they are using Halium (or something similar) here and not a mainline kernel to pull it off. Other than that, it's quite amazing - both hardware and the pre-release software._
* Thomas Triadi: [JingOS Arm64 build, running on RPi 3B. testing 01](https://www.youtube.com/watch?v=MsR5BvyVU9g). _Turns out: JingOS can run on a lot less powerful hardware, too! If you're interested in this, Thomas seems to have more videos on his channel for you._

#### Guadec 2021
* Bilal Elmoussaoui: [Writing applications using GTK 4 & Rust](https://www.youtube.com/watch?v=0preyzEb-fY&t=12284s).
* Tobias Bernard: [Adaptive Apps: The Future is Now](https://www.youtube.com/watch?v=xHqkiSd1hQQ&t=11040s). 

### Stuff I did

#### Content
* I have not been writing something new, only added another section to my old [Phosh 0.12 and its App Drawer refinement](https://linmob.net/phosh-0-12-app-drawer/) post. In other news, this web page has a media-query based dark mode now, and the code of [the theme](https://github.com/1peter10/minimola) used can be found on GitHub now.

#### Random

My past week was all about software:
* I forked [rokugtk](https://github.com/cliftonts/rokugtk) in order to modernize it a bit, and eventually needed help with that, which thanks to [shawdowwolf899](https://github.com/1peter10/rokugtk/commits?author=shadowwolf899) happened! Because free software is simply amazing, the resulting pull request has been accepted into the upstream project by now - only the ReadMe will need a bit more help. If you have a Roku, please do me a favour and test this by cloning the repo and running `./rokugtk.py`! _I might look into ways to make this more packagable in the future, pull requests welcome!_
* I also finally got around to playing with [modRana](https://modrana.org/trac) ([GitHub](https://github.com/M4rtinK/modrana)), a GPS navigation/Maps app. Because the [AUR PKGBUILD](https://aur.archlinux.org/packages/modrana/) is hopelessly outdated, I put some time into [creating a PKGBUILD](https://framagit.org/linmobapps/pkgbuilds) to build it straight from git - check the README for caveats! _Please contact me, best via Merge Request, if you have improvements!_

#### LINMOBapps

The number of apps surpassed a little milestone with 300 apps this week! I have continued to create some open issues to help with what to contribute with and to keep me focused - again, please feel free to add more issues, if you have some: Filing bugs/improvement requests are worthwhile contributions too! The website is also a bit lighter now, as I [removed some unnecessary CSS](https://framagit.org/linmobapps/linmobapps.frama.io/-/commit/d13f62798e5293892c179bebb78cca8e6992f95e).

These apps were added in the past week, upping the App count to 301:

* [YOGA Image Optimizer](https://github.com/flozz/yoga-image-optimizer), a graphical user interface for YOGA Image that converts and optimizes the size of JPEGs, PNGs and WebP image files;
* [dragonstone](https://gitlab.com/baschdel/dragonstone), another GTK Gemini and Gopher browser which fits the screen quite well;
* [OTPClient](https://github.com/paolostivanin/OTPClient), a highly secure and easy to use OTP client written in C/GTK that supports both TOTP and HOTP that supports importing from many other OTP apps; and
* [KMixtapez](https://codeberg.org/xaviers/KMixtapez), a Kirigami app for listening to mixtapes, albums & songs hosted on MyMixtapez.com.

I hope to focus on [backlog](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/4) in the coming week.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
